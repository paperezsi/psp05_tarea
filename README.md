# Tarea para PSP5
La tarea de la unidad esta dividida en 2 ejercicios.

## Ejercicio 1
Modifica el ejemplo del servidor HTTP (Proyecto java ServerHTTP, apartado 5.1 de los contenidos) para que incluya la cabecera Date.

## Instalación
Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp05_tarea.git
git checkout ejercicio01
```

Para la resolución de la tarea, se añade un nuevo método para obtener la fecha en el formato especificado y se añade una nueva linea en cada una de las cabeceras http que puede servir el servidor.

```
    //Metodo obtener y formatear la fecha
    public static String getDateValue() {
        DateFormat formatoFecha = new SimpleDateFormat("EEE, d MM yyyy HH:MM:ss z", Locale.ENGLISH);
        formatoFecha.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatoFecha.format(new Date());
    }
```

Configuración cabecera con la linea con Date

```
    //sirve la página
    html = Paginas.html_quijote;
    printWriter.println(Mensajes.lineaInicial_OK);
    printWriter.println(Paginas.primeraCabecera);
    printWriter.println("Content-Length: " + html.length() + 1);

    printWriter.println("Date: " + getDateValue());

    printWriter.println("\n");
    printWriter.println(html);
```

Para probar el correcto envio de las cabeceras con el campo Date, primero ejecuto el servidor desde el IDE y después en el navegador, en mi caso  mi caso Firefox Developer Edition consulto cada una de las urls para ver si ejecuta las páginas correctas.

![paso00.png](readme_src/tarea01_01.PNG)

![paso00.png](readme_src/tarea01_02.PNG)

![paso00.png](readme_src/tarea01_03.PNG)

Después descomento los cambios en el código que permiten enviar la cabecera con el campo Date y vuelvo a comprobar que en este caso si estea la cabecera en cada una de las páginas propuestas en el servidor Http.

![paso00.png](readme_src/tarea01_04.PNG)

![paso00.png](readme_src/tarea01_05.PNG)



## Ejercicio 2
Modifica el ejemplo del servidor HTTP (Proyecto java ServerHTTP, apartado 5.1 de los contenidos) para que implemente multihilo, y pueda gestionar la concurrencia de manera eficiente.

## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp05_tarea.git
git checkout ejercicio02
```

Para la resolución de la tarea, primeramente creo una nueva clase llamada HiloCliente que extienda de Thread, después en el método run de la clase ejecuto toda la funcionalidad para atender las peticiones de un cliente y al final cierro los recursos usados

```
   
//classe hilo
class HiloCliente extends Thread {

    //Variables
    private Socket socketCliente;
    private InputStreamReader flujoEntrada;
    BufferedReader flujoBufferEntrada;
    //Constructor que inicia un nuevo socket
    HiloCliente(Socket socketCliente) {
        this.socketCliente = socketCliente;
        this.start();
    }

    @Override
    public void run() {
        
        System.out.println("Atendiendo al cliente ");

        try {

            //Variables locales
            String peticion;
            String html;

            //Flujo de entrada
            flujoEntrada = new InputStreamReader(socketCliente.getInputStream());

            //Espacio en memoria para la entrada de peticiones
            flujoBufferEntrada = new BufferedReader(flujoEntrada);

            //Objeto de java.io para escribir 'línea a línea' en un flujo de salida
            PrintWriter printWriter = new PrintWriter(socketCliente.getOutputStream(), true);

            //Mensaje petición cliente
            peticion = flujoBufferEntrada.readLine();
            System.out.println(peticion);

            //Suprimimos todos los espacios en blanco que contenga
            peticion = peticion.replaceAll("\\s", "");

            //Si se trata de una petición 'GET' 
            if (peticion.startsWith("GET")) {

                //extrae la subcadena entre 'GET' y 'HTTP/1.1'
                peticion = peticion.substring(3, peticion.lastIndexOf("HTTP"));

                //si corresponde a la página de inicio
                if (peticion.length() == 0 || peticion.equals("/")) {

                    //sirve la página
                    html = Paginas.html_index;
                    printWriter.println(Mensajes.lineaInicial_OK);
                    printWriter.println(Paginas.primeraCabecera);
                    printWriter.println("Content-Length: " + html.length() + 1);
                    printWriter.println("Date: " + getDateValue());
                    printWriter.println("\n");
                    printWriter.println(html);

                } //si corresponde a la página del Quijote
                else if (peticion.equals("/quijote")) {

                    //sirve la página
                    html = Paginas.html_quijote;
                    printWriter.println(Mensajes.lineaInicial_OK);
                    printWriter.println(Paginas.primeraCabecera);
                    printWriter.println("Content-Length: " + html.length() + 1);
                    printWriter.println("Date: " + getDateValue());
                    printWriter.println("\n");
                    printWriter.println(html);

                } //en cualquier otro caso
                else {

                    //sirve la página
                    html = Paginas.html_noEncontrado;
                    printWriter.println(Mensajes.lineaInicial_NotFound);
                    printWriter.println(Paginas.primeraCabecera);
                    printWriter.println("Content-Length: " + html.length() + 1);
                    printWriter.println("Date: " + getDateValue());
                    printWriter.println("\n");
                    printWriter.println(html);
                }
            }

        } catch (Exception e) {
            System.out.println("Error:" + e);
        }finally{
            //Cierro la conexion y libero los recursos
            cerrarConexion();
        }
    }
```

Para probar ejecuto el servidor y lanzo varias pestañas llamando a la misma dirección del servidor http simulando varios clientes que se conectan a la vez.
![paso00.png](readme_src/tarea02_01.PNG)


## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)