package psp05_tarea;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 *
 * @author Pablo
 */

public class Mensajes {
    //Mensajes de cabecera que intercambia el Servidor con el Cliente según protocolo HTTP
    public static final String lineaInicial_OK = "HTTP/1.1 200 OK";
    public static final String lineaInicial_NotFound = "HTTP/1.1 404 Not Found";
}
