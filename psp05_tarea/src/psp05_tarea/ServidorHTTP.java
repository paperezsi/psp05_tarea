package psp05_tarea;

import java.io.BufferedReader;
import java.net.Socket;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * /**
 * Enunciado. Ejercicio 1: Modifica el ejemplo del servidor HTTP (Proyecto java
 * ServerHTTP, apartado 5.1 de los contenidos) para que incluya la cabecera
 * Date.
 *
 * *****************************************************************************
 * Servidor HTTP que atiende peticiones de tipo 'GET' recibidas por el puerto
 * 8066
 *
 * NOTA: para probar este código, comprueba primero de que no tienes ningún otro
 * servicio por el puerto 8066 (por ejemplo, con el comando 'netstat' si estás
 * utilizando Windows)
 *
 * @author Pablo
 */
class ServidorHTTP {

    //Variables
    private static final int PUERTO = 8066;
    private static int contador=0;

    //Método princiapal main
    public static void main(String[] args) throws IOException, Exception {
        try{
            //Creamos un serverSocket y le asignamos al servidor el puerto
            ServerSocket socketServidor = new ServerSocket(PUERTO);
            imprimeDisponible();
            Socket socketCliente;

            //bucle para atender las petición por el socket cliente
            while (true) {
                new HiloCliente(socketServidor.accept());
                System.out.println("cliente atendido");
                contador++;
            }
            
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
    }
    
    //Muestra un mensaje en la Salida que confirma el arranque, y da algunas 
    //indicaciones posteriores
    private static void imprimeDisponible() {

        System.out.println("El Servidor WEB se está ejecutando "
                + "y permanece a la escucha por el puerto " + PUERTO + ".");

        System.out.println("Escribe en la barra de direcciones de tu explorador preferido\n");
        System.out.println("http://localhost:8066\npara solicitar la página de bienvenida\n");
        System.out.println("http://localhost:8066/quijote\npara solicitar una página del Quijote\n");
        System.out.println("http://localhost:8066/q\npara simular un error\n");
        System.out.println("=================================================================\n");
    }

}
  
//classe hilo
class HiloCliente extends Thread {

    //Variables
    private Socket socketCliente;
    private InputStreamReader flujoEntrada;
    private BufferedReader flujoBufferEntrada;
    
    //Constructor que inicia un nuevo socket
    HiloCliente(Socket socketCliente) {
        this.socketCliente = socketCliente;
        this.start();
    }

    @Override
    public void run() {
        
        System.out.println("Atendiendo al cliente ");

        try {

            //Variables locales
            String peticion;
            String html;

            //Flujo de entrada
            flujoEntrada = new InputStreamReader(socketCliente.getInputStream());

            //Espacio en memoria para la entrada de peticiones
            flujoBufferEntrada = new BufferedReader(flujoEntrada);

            //Objeto de java.io para escribir 'línea a línea' en un flujo de salida
            PrintWriter printWriter = new PrintWriter(socketCliente.getOutputStream(), true);

            //Mensaje petición cliente
            peticion = flujoBufferEntrada.readLine();
            System.out.println(peticion);

            //Suprimimos todos los espacios en blanco que contenga
            peticion = peticion.replaceAll("\\s", "");

            //Si se trata de una petición 'GET' 
            if (peticion.startsWith("GET")) {

                //extrae la subcadena entre 'GET' y 'HTTP/1.1'
                peticion = peticion.substring(3, peticion.lastIndexOf("HTTP"));

                //si corresponde a la página de inicio
                if (peticion.length() == 0 || peticion.equals("/")) {

                    //sirve la página
                    html = Paginas.html_index;
                    printWriter.println(Mensajes.lineaInicial_OK);
                    printWriter.println(Paginas.primeraCabecera);
                    printWriter.println("Content-Length: " + html.length() + 1);
                    printWriter.println("Date: " + getDateValue());
                    printWriter.println("\n");
                    printWriter.println(html);

                } //si corresponde a la página del Quijote
                else if (peticion.equals("/quijote")) {

                    //sirve la página
                    html = Paginas.html_quijote;
                    printWriter.println(Mensajes.lineaInicial_OK);
                    printWriter.println(Paginas.primeraCabecera);
                    printWriter.println("Content-Length: " + html.length() + 1);
                    printWriter.println("Date: " + getDateValue());
                    printWriter.println("\n");
                    printWriter.println(html);

                } //en cualquier otro caso
                else {

                    //sirve la página
                    html = Paginas.html_noEncontrado;
                    printWriter.println(Mensajes.lineaInicial_NotFound);
                    printWriter.println(Paginas.primeraCabecera);
                    printWriter.println("Content-Length: " + html.length() + 1);
                    printWriter.println("Date: " + getDateValue());
                    printWriter.println("\n");
                    printWriter.println(html);
                }
            }

        } catch (Exception e) {
            System.out.println("Error:" + e);
        }finally{
            //Cierro la conexion y libero los recursos
            cerrarConexion();
        }
    }
    
    
    //Metodo obtener y formatear la fecha
    public String getDateValue() {
        DateFormat formatoFecha = new SimpleDateFormat("EEE, d MM yyyy HH:MM:ss z", Locale.ENGLISH);
        formatoFecha.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatoFecha.format(new Date());
    }

    //Método para cerrar la conexion y libero los recursos
    public void cerrarConexion(){
        try{
            
            if(flujoBufferEntrada != null){
                flujoBufferEntrada.close();
            }
            
            if(socketCliente != null){
                socketCliente.close();
            }
            
            System.out.println("HiloCliente Terminado");
        
        }catch(Exception e){
            System.out.println("Error:" + e);
        }
    }

}